## Find Largest Possible Product from List of Integers

Given a list of integers, return largest possible product from any combination.

Example:

Input: [-4, -2, -1, 0, 3, 55]

Output: 1320

Used number: -4, -2, 3, and 55