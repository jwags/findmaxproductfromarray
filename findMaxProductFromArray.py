# Basically, seperate all positive and negative numbers into two seperate lists.
# If the negative number list is odd, remove the largest one (one closest to zero).
# Multiple all numbers in both lists together. If both lists are empty, multiply
# all numbers in original list together. If positive list is empty and negative list
# has 1 element, multiply all elements in original list.
def solution(xs):
    negativeNumbers = []
    positiveNumbers = []
    for currentPannel in xs:
        if currentPannel > 0:
            positiveNumbers = positiveNumbers + [currentPannel]
        elif currentPannel < 0:
            negativeNumbers = negativeNumbers + [currentPannel]

    if len(positiveNumbers) == 0 and len(negativeNumbers) == 0:
       return str(multiplAllElementsInList(xs))
    if len(positiveNumbers) == 0 and len(negativeNumbers) == 1:
        return str(multiplAllElementsInList(xs))
    
    negativeNumbers.sort()
    if len(negativeNumbers) %2 != 0:
        del negativeNumbers[-1]
        
    return str(multiplAllElementsInList(negativeNumbers + positiveNumbers))
       
def multiplAllElementsInList(listToMultiply):
    if len(listToMultiply) == 0:
        return 0
    
    currentProduct = 1
    for item in listToMultiply:
        currentProduct = currentProduct * item
    return currentProduct
        
def test1():
    testData = [2, 0, 2, 2, 0]
    expected = "8"
    actual = solution(testData)
    print('Test 1')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')

def test2():
    testData = [-2, -3, 4, -5]
    expected = "60"
    actual = solution(testData)
    print('Test 2')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')

def test3():
    testData = [-2, -3, 4, -5, 0, 99, 8, -4, 55, -1, 1]
    expected = "20908800"
    actual = solution(testData)
    print('Test 3')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')

def test4():
    testData = [-1]
    expected = "-1"
    actual = solution(testData)
    print('Test 4')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')

test1()
test2()
test3()
test4()
